from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.shortcuts import render
from django.views import generic

from Consultation.models import ConsultationModel as Consultation
from Controle.models import ControleModel as Controle
from Patient.manager import PatientManager

import datetime as DT

class HomeView(LoginRequiredMixin, generic.base.TemplateView):
    template_name = 'home.html'
    def get_context_data(self, *args, **kwargs):
        context = super(HomeView, self).get_context_data()
        today = DT.datetime.now()
        td_1 = DT.timedelta(days=1)
        days = [
            today.date(),
            (today + td_1).date(),
            (today + td_1*2).date(),
            (today + td_1*4).date(),
            (today + td_1*5).date(),
            (today + td_1*6).date(),
            (today + td_1*7).date(),
            (today + td_1*8).date(),
            ]
        rdvsCtrl = dict()
        rdvsCons = dict()
        for day in days:
            consultations = Consultation.objects.filter(date_rdv = day)
            controles     = Controle.objects.filter(date_rdv = day)
            rdvsCons[day] = consultations
            rdvsCtrl[day] = controles
        context['rdvsCons'] = rdvsCons
        context['rdvsCtrl'] = rdvsCtrl
        context['dates']    = days
        return context

    def get_patient_consultation_rdv(self, date):
        queryset = Consultation.objects.filter(Q(date_rdv__exact = date))
