
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path, include
from .views import HomeView
urlpatterns = [
    path('', HomeView.as_view(), name='home'),
    path('patient/', include('Patient.urls', namespace='patient')),
    path('consultation/', include('Consultation.urls', namespace='consultation')),
    path('controle/', include('Controle.urls', namespace='controle')),
    path('admin/', admin.site.urls),
    path('accounts/login/', auth_views.LoginView.as_view(template_name='login.html')),
    path('accounts/', include('django.contrib.auth.urls')),
]
