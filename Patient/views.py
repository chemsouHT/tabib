from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.views import generic
from django.urls import reverse_lazy

from .api.pagination import PAGE_SIZE

from .forms import PatientCreateForm, PatientUpdateForm
from .models import PatientModel


class PatientListView(LoginRequiredMixin, generic.base.TemplateView):
    template_name = 'patient_list.html'
    def get_context_data(self, *args, **kwargs):
        context = super(PatientListView, self).get_context_data()
        context['page_size'] = PAGE_SIZE
        context['patient_list_api_url'] = reverse_lazy('patient:api:list')
        return context

class PatientCreateView(LoginRequiredMixin, generic.CreateView):
    template_name = 'patient_create.html'
    form_class = PatientCreateForm


class PatientDetailView(LoginRequiredMixin, generic.DetailView):
    template_name = 'patient_detail.html'
    def get_object(self, *args, **kwargs):
        patient_pk = self.kwargs.get('pk')
        return PatientModel.objects.get(pk=patient_pk)
    def get_context_data(self, *args, **kwargs):
        context = super(PatientDetailView, self).get_context_data()
        context['consultation_create_api_url'] = reverse_lazy('consultation:api:create')
        return context


class PatientUpdateView(LoginRequiredMixin, generic.UpdateView):
    template_name = 'patient_update.html'
    form_class = PatientUpdateForm
    def get_object(self, *args, **kwargs):
        patient_pk = self.kwargs.get('pk')
        return PatientModel.objects.get(pk=patient_pk)


class PatientDeleteView(LoginRequiredMixin, generic.DeleteView):
    template_name = 'patient_delete.html'
    success_url = reverse_lazy('patient:list')
    def get_object(self, *args, **kwargs):
        patient_pk = self.kwargs.get('pk')
        return PatientModel.objects.get(pk=patient_pk)
