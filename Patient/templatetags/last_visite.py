from django import template

register = template.Library()
from ..manager import PatientManager
# @register.filter(name='last_visite')
def last_visite(patient):
    date = PatientManager.get_last_visite(patient)
    if date is None:
        return 'Aucune'
    else:
        return date.strftime(' %d %B %Y ')

    return date


register.filter('last_visite', last_visite)
