from rest_framework import pagination

PAGE_SIZE = 10

class PatientListPagination(pagination.PageNumberPagination):
    page_size           = PAGE_SIZE
    page_query_param    = 'num_page'
