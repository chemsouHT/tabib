from rest_framework import serializers
import datetime
from ..models import PatientModel
from ..manager import PatientManager
class PatientSerializer(serializers.ModelSerializer):
    class Meta(object):
        fields = '__all__'
        model = PatientModel


class PatientListSerializer(PatientSerializer):
    age         = serializers.SerializerMethodField()
    last_visite = serializers.SerializerMethodField()
    detail_url  = serializers.SerializerMethodField()
    class Meta(object):
        model = PatientModel
        fields = [
        'nom',
        'prenom',
        'gendre',
        'profession',
        'telephone',

        'age',
        'last_visite',
        'detail_url',
        ]

    def get_age(self, object):
        return object.get_age()

    def get_detail_url(self, object):
        return object.get_detail_url()

    def get_last_visite(self, object):
        date = PatientManager.get_last_visite(object)
        if date is None:
            return 'Aucune'
        else:
            return date.strftime(' %d %B %Y ')
