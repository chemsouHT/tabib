from django.contrib import admin
from django.urls import path, include

from .views import PatientListAPIView


app_name = 'patient-api'

urlpatterns = [
    path('list/', PatientListAPIView.as_view(), name='list'),
]
