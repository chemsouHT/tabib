from django.db.models import Q
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated

from .serializer import PatientListSerializer
from ..models import PatientModel
from .pagination import PatientListPagination

import datetime

class PatientListAPIView(generics.ListAPIView):
    serializer_class = PatientListSerializer
    pagination_class = PatientListPagination
    permission_classes = [IsAuthenticated]

    def get_queryset(self, *args, **kwargs):
        query_nom = self.request.GET.get('nom', None)
        query_prenom = self.request.GET.get('prenom', None)
        query_age = self.request.GET.get('age', None)
        query_gendre = self.request.GET.get('gendre', None)
        query_telephone = self.request.GET.get('telephone', None)

        queryset = PatientModel.objects.all()
        if query_nom is not None:
            queryset = queryset.filter(Q(nom__icontains = query_nom))
        if query_prenom is not None:
            queryset = queryset.filter(Q(prenom__icontains = query_prenom))
        if query_age is not None:
            dt = datetime.datetime.today()
            date_naissance = datetime.datetime(year=dt.year-int(query_age), month=dt.month, day=dt.day)
            date_naissance_year = date_naissance.year
            dRange = [date_naissance_year-1, date_naissance_year, date_naissance_year+1]
            queryset = queryset.filter(Q(date_naissance__year__in = dRange))
        if query_gendre is not None:
            queryset = queryset.filter(Q(gendre__exact = query_gendre))
        if query_telephone is not None:
            queryset = queryset.filter(Q(telephone__istartswith = query_telephone))

        return queryset.order_by('-timestamp')
