from django.contrib import admin
from django.urls import path, include

from .views import PatientListView, PatientCreateView, PatientDetailView, PatientUpdateView, PatientDeleteView

app_name = 'Patient'

urlpatterns = [
    path('api/', include('Patient.api.urls', namespace='api')),
    path('list/', PatientListView.as_view(), name='list'),
    path('create/', PatientCreateView.as_view(), name='create'),
    path('<int:pk>/', PatientDetailView.as_view(), name='detail'),
    path('<int:pk>/update', PatientUpdateView.as_view(), name='update'),
    path('<int:pk>/delete', PatientDeleteView.as_view(), name='delete'),
]
