from django.db import models
from django.urls import reverse_lazy
# Create your models here.

import datetime


class PatientModel(models.Model):
    GENDER_CHOICES = (
        ('H', 'Homme'),
        ('F', 'Femme'),
    )
    nom             = models.CharField(max_length=150)
    prenom          = models.CharField(max_length=150)
    date_naissance  = models.DateField()
    gendre          = models.CharField(max_length=1, choices=GENDER_CHOICES, null=True)
    adresse         = models.CharField(max_length=255, null=True, blank=True)
    profession      = models.CharField(max_length=150, null=True, blank=True)
    telephone       = models.CharField(max_length=150, null=True, blank=True)

    timestamp       = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.nom + " " + self.prenom

    def get_age(self):
        age = datetime.datetime.now().date() - self.date_naissance
        return int(age.days/365.25)

    def get_detail_url(self):
        return reverse_lazy('patient:detail', kwargs={'pk':self.pk})

    def get_absolute_url(self):
        return self.get_detail_url()

    def get_update_url(self):
        return reverse_lazy('patient:update', kwargs={'pk':self.pk})

    def get_delete_url(self):
        return reverse_lazy('patient:delete', kwargs={'pk':self.pk})

    def get_consultations(self):
        return self.consultation_set.all().order_by('-date')

    def get_controles(self):
        return self.objects.get_controles()
