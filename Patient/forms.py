import datetime
from django import forms
from Tabib.forms import DateInput

from .models import PatientModel

class PatientCreateForm(forms.ModelForm):
    class Meta:
        model   = PatientModel
        fields = '__all__'
        labels = {
        'prenom' : 'Prénom',
        'date_naissance' : 'Date de naissance',
        'telephone' : 'N° Téléphone',
        }
        widgets = {
        'date_naissance' : DateInput(format='%Y-%m-%d'),
        }


class PatientUpdateForm(forms.ModelForm):
    class Meta:
        model   = PatientModel
        fields = '__all__'
        labels = {
        'prenom' : 'Prénom',
        'date_naissance' : 'Date de naissance',
        'telephone' : 'N° Téléphone',
        }
        widgets = {
        'date_naissance' : DateInput(format='%Y-%m-%d'),
        }
