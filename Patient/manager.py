from django.db import models

from Controle.models import ControleModel as Controle
from Consultation.models import ConsultationModel as Consultation

from .models import PatientModel

import datetime

class PatientManager(models.Manager):

    @classmethod
    def get_controles(self, patient):
        return Controle.objects.filter(models.Q(consultation__patient = patient))

    @classmethod
    def get_consultations(self, patient):
        return Consultation.objects.filter(models.Q(patient = patient))

    @classmethod
    def get_last_visite(self, patient):
        controles     = self.get_controles(patient)
        consultations = self.get_consultations(patient)
        last_controle = controles.filter(date = controles.aggregate(models.Max('date'))['date__max']).first()
        last_consultation = consultations.filter(date = consultations.aggregate(models.Max('date'))['date__max']).first()

        if last_controle is None and last_consultation is None:
            return None
        elif last_controle is None:
            return last_consultation.date
        elif last_consultation is None:
            return last_controle.date
        else:
            return max([last_consultation.date, last_controle.date])

    @classmethod
    def get_patients_consultation_rdv(self, date):
        return Consultation.objects.filter(models.Q(date_rdv = date))

    @classmethod
    def get_patients_controle_rdv(self, date):
        return Controle.objects.filter(models.Q(date_rdv = date))
