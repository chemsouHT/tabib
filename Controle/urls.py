from django.contrib import admin
from django.urls import path, include

from .views import ControleDetailView, ControleUpdateView, ControleDeleteView

app_name = 'Controle'

urlpatterns = [
    path('api/', include('Controle.api.urls', namespace='api')),
    path('<int:pk>/', ControleDetailView.as_view(), name='detail'),
    path('<int:pk>/update', ControleUpdateView.as_view(), name='update'),
    path('<int:pk>/delete', ControleDeleteView.as_view(), name='delete'),
]
