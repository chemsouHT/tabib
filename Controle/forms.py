from django import forms

from Tabib.forms import DateInput
from .models import ControleModel

class ControleUpdateForm(forms.ModelForm):

    class Meta:
        model = ControleModel
        fields = '__all__'
        exclude = ['consultation']
        labels = {
        'cat' : 'Conduite à tenir',
        'date_rdv' : 'Date du prochain Rendez-vous',
        'a_operer' : 'À opérer',
        }
        widgets = {
        'date' : DateInput(format='%Y-%m-%d'),
        'date_rdv' : DateInput(format='%Y-%m-%d'),
        'cat' : forms.Textarea( attrs={
                        'cols'          : "50",
                        'rows'          : "3",
                        'placeholder'   : 'Conduite à tenir ...',
                        }),
        'evolution' : forms.Textarea( attrs={
                        'cols'          : "50",
                        'rows'          : "3",
                        'placeholder'   : 'Evolution du patient ...',
                        })
        }
