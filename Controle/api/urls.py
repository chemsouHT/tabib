from django.contrib import admin
from django.urls import path, include

from .views import ControleCreateAPIView


app_name = 'controle-api'

urlpatterns = [
    path('create/', ControleCreateAPIView.as_view(), name='create'),
]
