from django.db.models import Q
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated

from .serializer import ControleSerializer

class ControleCreateAPIView(generics.CreateAPIView):
    serializer_class = ControleSerializer
    permission_classes = [IsAuthenticated]
