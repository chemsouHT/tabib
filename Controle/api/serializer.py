from rest_framework import serializers
import datetime
from ..models import ControleModel

class ControleSerializer(serializers.ModelSerializer):
    class Meta(object):
        fields = '__all__'
        model = ControleModel
