from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.views import generic
from django.urls import reverse_lazy
from .models import ControleModel

from .forms import ControleUpdateForm

class ControleDetailView(LoginRequiredMixin, generic.DetailView):
        template_name = 'controle_detail.html'
        def get_object(self, *args, **kwargs):
            controle_pk = self.kwargs.get('pk')
            return ControleModel.objects.get(pk=controle_pk)


class ControleUpdateView(LoginRequiredMixin, generic.UpdateView):
    template_name = 'controle_update.html'
    form_class = ControleUpdateForm
    def get_object(self, *args, **kwargs):
        controle_pk = self.kwargs.get('pk')
        return ControleModel.objects.get(pk=controle_pk)


class ControleDeleteView(LoginRequiredMixin, generic.DeleteView):
    template_name = 'controle_delete.html'
    success_url = reverse_lazy('controle:list')
    def get_object(self, *args, **kwargs):
        controle_pk = self.kwargs.get('pk')
        return ControleModel.objects.get(pk=controle_pk)
