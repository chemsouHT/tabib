from django.db import models
from django.urls import reverse_lazy
# Create your models here.
from Consultation.models import ConsultationModel

class ControleModel(models.Model):
    date        = models.DateField()
    evolution   = models.TextField(max_length=255)
    cat         = models.TextField(max_length=255)
    a_operer    = models.BooleanField(default=False, blank=True)
    date_rdv    = models.DateField()
    consultation= models.ForeignKey(ConsultationModel, on_delete=models.CASCADE, related_name='controle_set')

    timestamp       = models.DateTimeField(auto_now=True)


    def __str__(self):
        return 'Contrôle de la '+str(self.consultation)

    def get_num(self):
        consultation = self.consultation
        controle_set = consultation.controle_set.filter(models.Q(date__lte = self.date ))
        return len(controle_set)

    def get_detail_url(self):
        return reverse_lazy('controle:detail', kwargs={'pk':self.pk})

    def get_absolute_url(self):
        return self.get_detail_url()

    def get_update_url(self):
        return reverse_lazy('controle:update', kwargs={'pk':self.pk})

    def get_delete_url(self):
        return reverse_lazy('controle:delete', kwargs={'pk':self.pk})
