from django.contrib import admin
from django.urls import path, include

from .views import ConsultationDetailView, ConsultationUpdateView, ConsultationDeleteView

app_name = 'Consultation'

urlpatterns = [
    path('api/', include('Consultation.api.urls', namespace='api')),
    path('<int:pk>/', ConsultationDetailView.as_view(), name='detail'),
    path('<int:pk>/update', ConsultationUpdateView.as_view(), name='update'),
    path('<int:pk>/delete', ConsultationDeleteView.as_view(), name='delete'),
]
