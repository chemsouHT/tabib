from django.contrib import admin

# Register your models here.

from .models import ConsultationModel

admin.site.register(ConsultationModel)
