from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.views import generic
from django.urls import reverse_lazy

from .models import ConsultationModel
from .forms import ConsultationUpdateForm

class ConsultationDetailView(LoginRequiredMixin, generic.DetailView):
        template_name = 'consultation_detail.html'
        def get_object(self, *args, **kwargs):
            consultation_pk = self.kwargs.get('pk')
            return ConsultationModel.objects.get(pk=consultation_pk)
        def get_context_data(self, *args, **kwargs):
            context = super(ConsultationDetailView, self).get_context_data()
            context['controle_create_api_url'] = reverse_lazy('controle:api:create')
            return context


class ConsultationUpdateView(LoginRequiredMixin, generic.UpdateView):
    template_name = 'consultation_update.html'
    form_class = ConsultationUpdateForm
    def get_object(self, *args, **kwargs):
        consultation_pk = self.kwargs.get('pk')
        return ConsultationModel.objects.get(pk=consultation_pk)


class ConsultationDeleteView(LoginRequiredMixin, generic.DeleteView):
    template_name = 'consultation_delete.html'
    success_url = reverse_lazy('consultation:list')
    def get_object(self, *args, **kwargs):
        consultation_pk = self.kwargs.get('pk')
        return ConsultationModel.objects.get(pk=consultation_pk)
