from django import forms

from Tabib.forms import DateInput
from .models import ConsultationModel

class ConsultationUpdateForm(forms.ModelForm):

    class Meta:
        model = ConsultationModel
        fields = '__all__'
        exclude = ['patient']
        labels = {
        'cat' : 'Conduite à tenir',
        'date_rdv' : 'Date du prochain Rendez-vous',
        'a_operer' : 'À opérer',
        'signe_foncionnel' : 'Signe Fontionnel'
        }
        widgets = {
        'date' : DateInput(format='%Y-%m-%d'),
        'date_rdv' : DateInput(format='%Y-%m-%d'),
        'cat' : forms.Textarea( attrs={
                        'cols'          : "50",
                        'rows'          : "3",
                        'placeholder'   : 'Conduite à tenir ...',
                        }),
        'signe_foncionnel' : forms.Textarea( attrs={
                        'cols'          : "50",
                        'rows'          : "3",
                        'placeholder'   : 'Signe fontionnel du patient ...',
                        }),
        'signe_physique' : forms.Textarea( attrs={
                        'cols'          : "50",
                        'rows'          : "3",
                        'placeholder'   : 'Signe physique du patient ...',
                        }),
        'diagnostic' : forms.Textarea( attrs={
                        'cols'          : "50",
                        'rows'          : "3",
                        'placeholder'   : 'Diagnostic du médecin ...',
                        }),
        'biologie' : forms.Textarea( attrs={
                        'cols'          : "50",
                        'rows'          : "3",
                        'placeholder'   : 'Biologie ...',
                        }),
        'radiologie' : forms.Textarea( attrs={
                        'cols'          : "50",
                        'rows'          : "3",
                        'placeholder'   : 'Radiologie ...',
                        }),
        'autre' : forms.Textarea( attrs={
                        'cols'          : "50",
                        'rows'          : "3",
                        'placeholder'   : 'Autre ...',
                        }),
        }
