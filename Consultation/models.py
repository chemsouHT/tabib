from django.db import models
from django.urls import reverse_lazy
# Create your models here.
from Patient.models import PatientModel

class ConsultationModel(models.Model):

    date       = models.DateField()
    patient    = models.ForeignKey(PatientModel, on_delete=models.CASCADE, related_name='consultation_set')

    #Clinique
    motif           = models.CharField(max_length=255)
    signe_foncionnel= models.TextField(max_length=255, null=True, blank=True)
    signe_physique  = models.TextField(max_length=255, null=True, blank=True)
    biologie        = models.TextField(max_length=255)
    radiologie      = models.TextField(max_length=255)
    autre           = models.TextField(max_length=255, null=True, blank=True)

    #Diagnostique
    diagnostic      = models.TextField(max_length=255)

    #CAT
    cat             = models.TextField(max_length=255)
    a_operer        = models.BooleanField(default=False, blank=True)

    #RDV
    date_rdv        = models.DateField()

    timestamp       = models.DateTimeField(auto_now=True)

    def __str__(self):
        return 'Consultation de '+str(self.patient)+' / '+str(self.get_num())

    def get_num(self):
        patient = self.patient
        consultaion_set = patient.consultation_set.filter(models.Q(date__lte = self.date ))
        return len(consultaion_set)

    def get_antecedents(self):
        patient = self.patient
        consultaion_set = patient.consultation_set.filter(models.Q(date__lt = self.date ))
        return consultaion_set.order_by('-timestamp')

    def get_detail_url(self):
        return reverse_lazy('consultation:detail', kwargs={'pk':self.pk})

    def get_absolute_url(self):
        return self.get_detail_url()

    def get_update_url(self):
        return reverse_lazy('consultation:update', kwargs={'pk':self.pk})

    def get_delete_url(self):
        return reverse_lazy('consultation:delete', kwargs={'pk':self.pk})

    def get_controles(self):
        return self.controle_set.all().order_by('-date')
