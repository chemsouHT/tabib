from django.db.models import Q
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from .serializer import ConsultationSerializer

class ConsultationCreateAPIView(generics.CreateAPIView):
    serializer_class = ConsultationSerializer
    permission_classes = [IsAuthenticated]
