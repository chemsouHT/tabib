from django.contrib import admin
from django.urls import path, include

from .views import ConsultationCreateAPIView


app_name = 'consultation-api'

urlpatterns = [
    path('create/', ConsultationCreateAPIView.as_view(), name='create'),
]
