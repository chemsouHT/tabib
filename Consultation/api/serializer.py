from rest_framework import serializers
import datetime
from ..models import ConsultationModel

class ConsultationSerializer(serializers.ModelSerializer):
    class Meta(object):
        fields = '__all__'
        model = ConsultationModel
