var postControleCreateUrl;
function controle_create_init(url) {
  postControleCreateUrl = url;
  controle_create_listeners();
}


function controle_create_listeners() {
  $('#controle-create-form').submit(function(event) {
    event.preventDefault();
    var form = $(this).serialize();
    creerControle(form);
  })
}

function creerControle(data) {
  $.ajax({
       type       : "POST",
       url        : postControleCreateUrl,
       data       : data,
       success    : function(data){
         alert('Contrôle créé avec succès');
         location.reload();
       }
     });
}
