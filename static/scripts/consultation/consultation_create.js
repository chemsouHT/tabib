var postConsultationCreateUrl;
function consultation_create_init(url) {
  postConsultationCreateUrl = url;
  consultation_create_listeners();
}


function consultation_create_listeners() {
  $('#consultation-create-form').submit(function(event) {
    event.preventDefault();
    var form = $(this).serialize();
    creerConsultation(form);
  })
}

function creerConsultation(data) {
  $.ajax({
       type       : "POST",
       url        : postConsultationCreateUrl,
       data       : data,
       success    : function(data){
         alert('Consultation créée avec succès');
         location.reload();
       }
     });
}
