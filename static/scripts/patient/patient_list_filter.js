var FILTER_TOGGLED = false

function decdetFilterInit() {
  decdetFilterListeners();
}

function decdetFilterListeners() {
    $('#patient-filter-toggle-btn').on('click', function(event) {
        event.preventDefault();
        if(FILTER_TOGGLED){
          FILTER_TOGGLED = false
          $('#patient-list-filter-form').slideUp();
        }else{
          FILTER_TOGGLED = true
          $('#patient-list-filter-form').slideDown();
        }
    })
}
