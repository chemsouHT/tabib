var paginationSize;
var fetchPatientsUrl;
var nextPatientsDataUrl;
var previousPatientsDataUrl;
var patientsCount;

var query = [];
var pageNum = 1;
var patientListTBody = $ ('#patient_list-tbody');

function init_patient_list(url, pagination){
  fetchPatientsUrl = url;
  paginationSize = pagination;
  fetchPatientsAjax();
  listeners();
}


function fetchPatientsAjax() {
  $.ajax({
       type       : "GET",
       url        : fetchPatientsUrl,
       data       : query,
       success    : function(data){
         nextPatientsDataUrl     = data.next;
         previousPatientsDataUrl = data.previous;
         patientsCount           = data.count;
         parsePatients(data.results);
         updateDataNavigationBtns();
         if(data.results)
            updateDecDetsInfo(data.results.length);
       }
     });
}


//decDetList in JSON
function parsePatients(patients) {
  if (!patientsCount) {
    displayEmpty();
  } else {
    $('#display-empty').text('');
    $('#patient-list-div').slideDown();

    $.each(patients, function(key,object){
        attachPatient(object);
      });
  }
}


function displayEmpty() {
  $('#display-empty').text('Aucun patient à afficher');
  $('#patient-list-div').slideUp();
}

function attachPatient(patient) {
  var tr = $('<tr></tr>');
  var tdNom = $('<td></td>').text(patient.nom);
  var tdPrenom = $('<td></td>').text(patient.prenom);
  if(patient.gendre == 'F')
    var tdGendre = $('<td></td>').append('<i class="fas fa-venus"></i> Femme');
  else
    var tdGendre = $('<td></td>').append('<i class="fas fa-mars"></i> Homme');

  var tdAge = $('<td></td>').text(patient.age + ' ans');
  var tdDernVisite = $('<td></td>').text(patient.last_visite);
  var aDetail  = $('<a></a>').append('<i class="fas fa-user-injured fa-2x"></i>')
                      .attr({
                         href : patient.detail_url,
                         'data-toggle' : "tooltip",
                         'data-placement' : "left",
                          title : "Patient "+patient.nom+" "+patient.prenom
                       });
  var tdActions = $('<td></td>').append(aDetail);
  tr.append(tdNom, tdPrenom, tdGendre, tdAge, tdDernVisite, tdActions);
  patientListTBody.append(tr);
}


function refetchData() {
    $('#patient-list-div').slideUp(function functionName() {
      patientListTBody.text("");
      fetchPatientsAjax();
      $('#patient-list-div').slideDown();
    });
}

function updateDataNavigationBtns() {
    var nextDataNavigationBtn = $('#navigation-fetch-data-btn-next');
    var previousDataNavigationBtn = $('#navigation-fetch-data-btn-previous');
    if(nextPatientsDataUrl == null) nextDataNavigationBtn.attr('disabled', true);
    else nextDataNavigationBtn.attr('disabled', false);
    if(previousPatientsDataUrl == null) previousDataNavigationBtn.attr('disabled', true);
    else previousDataNavigationBtn.attr('disabled', false);
}

function updateDecDetsInfo(sizeShown) {
    $('#patient-list-html-info').text(
            'Affichage de '+ sizeShown +' patient(s) sur '+ patientsCount +
            '        |     Page n°' + pageNum + ' / ' + (Math.ceil(patientsCount / paginationSize))
        );
}


//Listeners

function listeners() {

    $('#navigation-fetch-data-btn-next').on('click', function(){
        fetchPatientsUrl = nextPatientsDataUrl;
        refetchData();
        pageNum++;
    });

    $('#navigation-fetch-data-btn-previous').on('click', function(){
        fetchPatientsUrl = previousPatientsDataUrl;
        refetchData();
        pageNum--;
    });

    $('#patient-list-filter-form').on('submit', function(event) {
        event.preventDefault();
        var formFields = $(this).find('input,select');
        query = [];
        $.each(formFields, function(key,object){
          if(object.name && object.value)
            query.push({
              name   : object.name,
              value  : object.value
            });
        });
        refetchData();
    });

    $('#patient-list-filter-form').on('reset', function(event) {
        query = [];
        refetchData();
    });
}
